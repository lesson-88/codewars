import { duplicateEncode } from './solution';
import { describe, it, expect } from 'vitest';

const acceptTheseParams = ['', ' ', '-', 'abc'];
const refuseTheseParams = [
  null,
  true,
  undefined,
  12,
  12.3,
  () => {},
  {}
];
const examples = [
  ['abc', '((('],
  ['test message', '))))(()))(()'],
  ['din', '((('],
  ['recede', '()()()'],
  ['Success', ')())())']
]

describe('Duplicate Encoder', () => {
  describe('duplicateEncode function', () => {

    it('should exist', () => {
      expect(typeof duplicateEncode).toEqual('function');
    });

    describe('should refuse these params', () => {
      refuseTheseParams.map(param => {
        it(`refuse ${param}`, () => {
          let err = '';

          try {
            duplicateEncode(param);
          } catch ({ message }) {
            err = message;
          }

          expect(err).toEqual('Param must be a string!')
        });
      })
    });

    describe('should accept these params', () => {
      acceptTheseParams.map(param => {
        it(`accept ${param}`, () => {
          let err = '';

          try {
            duplicateEncode(param);
          } catch ({ message }) {
            err = message;
          }

          expect(err).toEqual('');
        });
      });
    });

    describe('must return a string', () => {
      examples.map(([example]) => {
        it(`param ${example}`, () => {
          expect(typeof duplicateEncode(example)).toEqual('string');
        });
      });
    });

    describe('solve the examples', () => {
      examples.map(([example, result]) => {
        it(`example ${example}`, () => {
          expect(duplicateEncode(example)).toEqual(result);
        });
      });
    });
  })
});