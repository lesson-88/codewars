// https://www.codewars.com/kata/514b92a657cdc65150000006/train/javascript

export const isDivisibleByThree = (number) => number % 3 === 0

export const isDivisibleByFive = (number) => number % 5 === 0

export const sum = (previous, current) => previous + current

export const solution = number => {
    if (number < 0) return 0

    return new Array(number).fill(0).map((v, i) => i)
        .filter((number) => isDivisibleByThree(number) || isDivisibleByFive(number))
        .reduce(sum, 0)
}