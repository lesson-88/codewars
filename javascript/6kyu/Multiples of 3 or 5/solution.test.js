import { describe, expect, it } from "vitest"
import { solution } from "./solution"

describe("basic tests", function(){
    it.each([
        [10, 23],
        [14, 45],
    ])(`Number: %i`, (param, expected) => {  
        let actual = solution(param)
        expect(actual).toEqual(expected)
    })
})