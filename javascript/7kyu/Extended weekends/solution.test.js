import { describe, it, expect } from 'vitest';
import {
	extendedWeekend,
	isTheMonthHas31Days,
	isFridayTheFirstDay
} from './solution';
import {alphabetPosition} from "../../6kyu/Replace With Alphabet Position/solution";

describe('Extended weekends', () => {
	describe('isTheMonthHas31Days should', () => {
		it('calculate the number of days of the selected month', () => {
			const returnValue = isTheMonthHas31Days(2020, 0)
			expect(returnValue).toBeTruthy()
		})
	});
	describe('isFridayTheFirstDay', () => {
		it('check the first day is Friday or not', () => {
			const returnValue = isFridayTheFirstDay(2019,1)
			expect(returnValue).toBeTruthy()
		})
	})
	describe('extendedWeekend should', () => {
		it('exists', () => {
			expect(extendedWeekend).toBeTruthy();
		})
		it('accept `startYear` as integer value only', () => {
			const fn = () => extendedWeekend(null, null)

			try {
				fn();
				expect().toThrowError('Should refuse all the non string params!');
			} catch ({ message }) {
				expect(message).toEqual('The param of "startYear" is invalid!');
			}
		})
		it('accept `endYear` as integer value only', () => {
			const fn = () => extendedWeekend(2013, null)

			try {
				fn();
				expect().toThrowError('Should refuse all the non string params!');
			} catch ({ message }) {
				expect(message).toEqual('The param of "endYear" is invalid!');
			}
		})
		it('return an array', () => {
			const returnValue = extendedWeekend(2013, 2016);
			expect(Array.isArray(returnValue)).toBeTruthy();
		})
		it('calculate the right value', () => {
			const [firstMonth, lastMonth, sum] = extendedWeekend(2019, 2020);

			expect(firstMonth).toEqual('Mar');
			expect(lastMonth).toEqual('May');
			expect(sum).toEqual(2);
		})
	});
});