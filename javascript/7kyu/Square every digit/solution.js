// https://www.codewars.com/kata/546e2562b03326a88e000020/train/javascript

const solution = number => {
	if (!Number.isInteger(number)) throw new Error('The param of `number` is invalid!');

	return number
		.toString()
		.split('')
		.map(char => (char * char).toString())
		.reduce((previous, current) => previous += current, '') * 1
}

const squareDigits = num => solution(num)

module.exports = {
	solution
}