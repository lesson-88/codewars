import { describe, it, expect } from 'vitest';
import { solution } from './solution';

describe('Square every digit', () => {
	describe('Solution should', () => {
		it('exists', () => {
			expect(solution).toBeTruthy();
		});

		it('accept only integers', () => {
			const paramVariations = [undefined, null, 1.2, '12', 'something', () => {}, [], {}, true];
			paramVariations.map(param => {
				const fn = () => solution(param);

				try {
					fn();
					expect().toThrowError('Should refuse all the non valid params!');
				} catch ({ message }) {
					expect(message).toEqual('The param of `number` is invalid!');
				}
			});
		});

		it('returns integer', () => {
			const returnValue = solution(12);
			expect(Number.isInteger(returnValue)).toBeTruthy();
		});

		it('make square every digits', () => {
			const check = [
				{param: 1, result: 1},
				{param: 11, result: 11},
				{param: 12, result: 14},
				{param: 13, result: 19},
				{param: 20, result: 40},
				{param: 22, result: 44},
				{param: 195, result: 18125},
			];
			check.map(({ param, result }) => {
				const returnValue = solution(param);
				expect(result).toEqual(returnValue);
			})
		});
	});
});