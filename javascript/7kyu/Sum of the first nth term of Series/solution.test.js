import { describe, it, expect } from 'vitest';
import { SeriesSum } from './solution';
const acceptableNumbers = [0, 1, 2, 19, 155];
const notAcceptableNumber = [-1, 0.1];
const invalidParams = ['', null, false, { test: 'nothing' }, ['value'], () => 'function'];

describe('Sum of the first nth term of Series', () => {
  describe('SeriesSum should', () => {
    
    it('exists', () => {
      expect(typeof SeriesSum).toEqual('function');
    });

    describe('accept only natural numbers', () => {
      acceptableNumbers.map(n => {
        it('accept \'' + n + '\'', () => {
          try {
            SeriesSum(n);
          } catch {
            expect('Should not throw error!').toThrowError();
          }
        });
      });
    });
    
    describe('refuse invalid params and not natural numbers', () => {
      [...invalidParams, ...notAcceptableNumber].map(n => {
        it('refuse \'' + n + '\'', () => {
          try {
            SeriesSum(n);
          } catch ({ message }) {
            expect(message).toEqual('Should refuse not natural numbers!');
          }
        });
      });
    });
    
    it('return zero keeping two decimals', () => {
      const number = 0;
      const returnValue = SeriesSum(number);

      expect(returnValue).toEqual('0.00');
    });

    it('accomplish the final tests', () => {
      expect(SeriesSum(1)).toEqual("1.00")
      expect(SeriesSum(2)).toEqual("1.25")
      expect(SeriesSum(3)).toEqual("1.39")
    });

  });
});