import { describe, it, expect } from 'vitest';
import { sumTwoSmallestNumbers } from './solution';

const acceptTheseParams = [
  [1, 3],
  [1, 2, 3]
];
const refuseTheseParams = [
  null,
  true,
  undefined,
  12,
  12.3,
  'something',
  () => {},
  {}
];
const refuseInvalidParams = [
  [1, 2, null],
  [1, 2, true],
  [1, 2, undefined],
  [1, 2, 12.1],
  [1, 2, 'something'],
  [1, 2, () => {}],
  [1, 2, []]
];

describe('Sum of two lowest positive integers', () => {
  describe('sumTwoSmallestNumbers function', () => {

    it('should exist', () => {
      expect(typeof sumTwoSmallestNumbers).toEqual('function');
    });

    describe('should accept these parameters', () => {
      acceptTheseParams.map(param => {
        it(`accept ${param}`, () => {
          let err = '';

          try {
            sumTwoSmallestNumbers(param);
          } catch ({ message }) {
            err = message;
          }

          expect(err).toEqual('');
        });
      });
    });

    describe('should refuse these parameters', () => {
      refuseTheseParams.map(param => {
        it(`refuse ${param}`, () => {
          let err = '';
          
          try {
            sumTwoSmallestNumbers(param);
          } catch ({ message }) {
            err = message;
          }
          
          expect(err).toEqual('Param must be an array!');
        })
      })
    });

    describe('should refuse invalid parameters', () => {
      refuseInvalidParams.map(param => {
        it(`refuse ${param}`, () => {
          let err = '';

          try {
            sumTwoSmallestNumbers(param);
          } catch ({ message }) {
            err = message;
          }

          expect(err).toEqual('Param must be an array of integers with minimum length of 2!');
        });
      });
    });

    it('should add together the two lowest numbers', () => {
      expect(sumTwoSmallestNumbers([5, 8, 12, 19, 22])).toEqual(13);
      expect(sumTwoSmallestNumbers([15, 28, 4, 2, 43])).toEqual(6);
      expect(sumTwoSmallestNumbers([3, 87, 45, 12, 7])).toEqual(10);
      expect(sumTwoSmallestNumbers([23, 71, 33, 82, 1])).toEqual(24);
      expect(sumTwoSmallestNumbers([52, 76, 14, 12, 4])).toEqual(16);
      expect(sumTwoSmallestNumbers([-1, 1, 2, 3])).toEqual(0);
    });

  });
})